---
title: Insider Alert! Mind Your Health
date: 2019-10-02 20:23:21
thumbnail: gallery/header-mango.jpeg
tags:
- Info
- Feedback
categories:
- World
- Info
- Health
---

This post is about the news article “Global Warming Pushes Microbes into Damaging Climate Feedback Loops” published by inside climate news on June 19 2019. This article gives us the idea about the impact of global warming on microbes and how microbes change the climate because of effects from global warming. Life on earth started from microbes. Microbes are very important for life on earth as they cycle minerals and nutrients through soil, water and atmosphere. “Microbes literally support all life on earth”. This is a line said by Tom Crowther one of 30 microbiologists who studied about microbes and climate change and published a paper on it. I do think this is true as microbes are one of the key aspects of nutrient cycle like nitrogen cycle. When bacteria feeds on organic matter they release CO2. This CO2 is a greenhouse gas and is responsible for global warming. Global Warming speeds up bacterial feasting of organic matter and release more CO2 and this in turn increases global temperature. Tom Crowther’s studies has shown that accelerated microbial activity in soils will greatly increase carbon emission by 2050. Spreading of crop diseases threatening food security is on warning sign.

## It's a Health Issue

Global warming helps spread of diseases rapidly. It is responsible for the spread of malaria to higher elevation of Africa, spread of bluetongue a livestock disease said Baylis one of the microbiologist. As environment warms, pathogens can multiply rapidly in new habitats previously too cold. Warming Oceans changes currents and can cause events like El Nino. El Nino can cause spread of diseases like cholera. Mainly water-borne diseases are spread by El Nino as temperature of the water body increases.

## Microbes Changes Affect Ocean Food Chain

In the Southern Ocean around Antartica, microplankton take in about 40 percent of the carbon sequestered by the oceans and sink it into the seafloor. This partly decreases the buildup of greenhouse gases. Antje Boetius who is a marine microbiologist observed that the widely reported extreme low Artic sea ice extent in the summer of 2012 rippled through the ocean’s ecosystems. Huge amount of microbes in the form of diatoms floating in sea drifted to the bottom in Greenland. Antje found that she observed a noticeable change in the ocean chemistry the dead microbes piled up at the ocean floor. This shows the sudden change in climate because of the microbes. Food and breeding of most Artic species are dependent on location and time of plankton blooms so a change in the ocean microbe cycles can affect the whole food chain.
