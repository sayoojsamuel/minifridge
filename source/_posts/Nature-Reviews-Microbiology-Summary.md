---
title: Nature Reviews Microbiology Summary
date: 2019-09-16 15:35:04
thumbnail: gallery/header-tree.jpg
tags:
  - Nature
  - Warning
  - Microorganisms
  -
author: Ananthu M P
---

All life on Earth evolved from microorganisms in the primordial slime, and billions of years later, the planet's smallest life forms (including bacteria, plankton and viruses) are still fundamental to the biosphere. They cycle minerals and nutrients through soil, water and the atmosphere. They help grow and digest the food we eat. Without microbes, life as we know it wouldn't exist.

The real impact of climate change depends on tiny organisms we can’t even see, argues an international panel of more than 30 microbiologists in a consensus statement published on  18-06-2019 in the journal Nature Reviews Microbiology
Microbes, or microorganisms, are any organism or virus invisible to the naked eye. Numbering in the nonillions (10 followed by 30 zeroes), they make up the “unseen majority” of life on Earth, according to the scientists. Microbes not only contribute to how fast the climate changes, but to our ability to mitigate and adapt to it.

# Climate Contributions

The world’s dirt holds on to some 2.2 trillion tons of carbon. That’s more than the combined amount of carbon in the atmosphere and in vegetation. And microbes controls how much carbon soil can hang on to and how much it releases.
Although plants remove CO2 from the atmosphere during food-making photosynthesis, they also release the greenhouse gas back into the atmosphere during respiration. The same is true of soil microbes. In terrestrial environments, microbes release a range of important greenhouse gases to the atmosphere (carbon dioxide, methane and nitrous oxide), and climate change is causing these emissions to increase
Now, global warming is supercharging some microbial cycles on a scale big enough to trigger damaging climate feedback loops, research is showing. Bacteria are feasting on more organic material and produce extra carbon dioxide as the planet warms. In the Arctic, a spreading carpet of algae is soaking up more of the sun's summer rays, speeding melting of the ice.
But there are microbes other than the ones in soil that impact climate change. Cows, sheep, goats and other similar animals have microbes in their guts that help the animals digest their food. However, as a by-product of this digestion, these microbes produce methane a potent greenhouse gas whose levels in the atmosphere have ballooned in the last five years.
Rice fields, too, release methane into the atmosphere
Rice is a major staple food for half the world’s population, so these methane emissions add up, Rice fields currently contribute about 20 percent of agriculture’s emissions of the greenhouse gas. As the population continues to grow, scientists anticipate those numbers will grow, too.

# Mitigation

"Microbes literally support all life on Earth," said Tom Crowther, an environmental scientist with ETH Zürich, who was among the signers of the statement. "Maintaining and preserving these incredible communities has to be our highest priority if we intend to maintain the existence that we want on this planet."

While microorganisms contribute substantially to climate change, the authors point out that these invisible creatures can also do the opposite. Agricultural fertilizers release nitrous oxide (N2O, more commonly known as laughing gas), another powerful greenhouse gas. But some bacteria that live in plant roots produce an enzyme that can convert N2O into nitrogen, a non-greenhouse gas that makes up nearly 80 percent of the air we breathe. Researchers say using bacteria that have higher levels of this enzyme can lower N2O emissions.

Fungal proteins can replace meat, lowering dietary carbon footprints. The authors also note breeding programs for cows that change their gut microbes to produce less methane could reduce emissions.  Understanding more about how microorganisms influence climate change could have further impacts on mitigation strategies, the researchers say.


