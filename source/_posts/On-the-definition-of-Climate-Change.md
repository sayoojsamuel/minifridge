---
title: On the definition of Climate Change
thumbnail: gallery/header-smoke.jpg
date: 2019-09-17 11:27:59
tags:
- Definition
- Climate Change
- Global Warming
author: Sayooj Samuel
---

## One about Climate Change

Climate Change is the development of drasting changes in climate systems resulting in new weather patterns. Melting of polar ice sheets, heavy rain and floods are some of the global demonstrations due to this phenomenon. 

## One about Global Warming

The blanket of gases which is formed due to green house gases. This thickening layer of gases traps more heat in the atmosphere causing the planet to warm up. 

## One about Green House Gases

The gases responsible for Global Warming are called Green House Gases.  The major gases are carbon dioxide, methane and nitrous oxide. Human activities are the main cause for the dramatic increase of these gases in the atmostphere. 
