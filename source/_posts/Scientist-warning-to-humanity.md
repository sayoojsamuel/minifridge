---
title: Scientist warning to humanity
date: 2019-09-13 18:02:43
thumbnail: gallery/header-lab.jpg
tags:
- Scientist
- Warning
- Microorganisms
author: sayoojsamuel
---

# The `Scientists' warning to humanity` Review

    In the Anthropocene, in which we now live, climate change is impacting most life on Earth. Microorganisms support the existence of all higher trophic life forms. To understand how humans and other life forms on Earth (including those we are yet to discover) can withstand anthropogenic climate change, it is vital to incorporate knowledge of the microbial ‘unseen majority’. We must learn not just how microorganisms affect climate change but also how they will be affected by climate change and other human activities...

This extract from one of the recent and hot article from ['Nature Reviews Microbiology'](https://www.nature.com/articles/s41579-019-0222-5), titled "Scientists’ warning to humanity: microorganisms and climate change",  set the begining of a new perspective of the importance of **Microorganisms** as a deciding factor in **Climate Change**.

33 Microbiologists coauthored this research and concluded that 

>the invisible, intangible, abundant and diverse microorganisms constitute the life support system of the biospehere

