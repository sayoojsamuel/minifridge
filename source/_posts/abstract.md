---
title: Microorganisms and Climate Change - Abstract
slug: microorganisms-abstract
thumbnail: gallery/abstract-header.jpg 
date: 2019-09-02 16:02:58
categories:
- Project
tags: 
- Abstract
---

## Abstract

Our project is based on the idea of Critic Posts on the topics related to ‘Microorganisms and Climate Change’. Microorganisms support the existence of all life forms.  And climate change is  an inevitable part of our future sustainable development goals.  The main motive that drives our project objective is the correlation between them, vastly divided by the concept, but strongly adhered by science.  In short, even the smallest of microorganisms greatly affects the climate change, and vice versa.  This codependent cycle has been explored by great minds from the early age, and some recent developments ending up with CLAW and ANTI-CLAW theories.  We hope to extent to this through our project.  

The things we expect to do as part of this project 
- Learn more about topic through reading Current Affairs
- Do research on the news
- Critic on the News Related to topic
- Post a Custom Blog from the acquired  knowledge
- Debunk Myths and fake news related to the topic
- Comment on Memes and Posts in Social Media.

With the completion of the project we expect to acquire a library of information about the effects  of microorganisms on climate change. By learning more about the topic from reading current happenings, facts and posts from social media, we will get a large assemblage of information.  We wish to share this information not only to our colleagues but also to the world through our blog and presentation.

### Team

This project is maintained by a bunch of 3rd year Computer Science and Engineering students from Amrita University, Amritapuri.
* Sayooj Samuel ([@sayoojsamuel](https://twitter.com/sayoojsamuel))
* Ananthu MP ()
* Rahul PV ()

This project would not have been possible without the full support and guideline from Sir GeVargis M T.

