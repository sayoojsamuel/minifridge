---
title: Unraveling Climate Feedback Loops
date: 2019-09-22 16:31:51
thumbnail: gallery/header-bacteria.jpg
tags:
- Climate
- Bacteria
- Feedback Loops
categories:
- Review
---

This blog post is a review of "Global Warming Pushes Microbes into Damaging Climate Feedback Loops", 

Are you aware of this weird theory by British biologist Thomas Henry Huxley on our origin? Bathybius haeckelii was a substance that he discovered and initially believed to be a form of primordial matter, a source of all organic life.

Microorganisms are still fundamental to the biosphere - bacteria, planktons, viruses. Life as we know woudn't exist. They are essentially everywhere - helping to grow and digest our food or even to cycle the nutrients. 

## Permafrost

If you are still ignorant of permafrost, its high time to get worried of this. Permafrost is a ground that remains completely frozen. But why is this so discusses? Can it be more dangerous? Unlike any other natural terrain, permafrost can 
- Ancient viruses and bacteria thaws (some dates back to 400,000 years)
- Newly unforzen bacteria's can make humans and animals sick

But the climate feedback loop?
This part was hard guess. When permaforst is frozen, plant material in the soil - the organic carbon can't decompose. As permafrost thaws, microbes start to decompose  this material. This process releases greenhouse gases like CO2, Methane to the atmosphere.

## Resources 

https://microbiologyonline.org/about-microbiology/microbes-and-climate-change/climate-change-and-health

https://insideclimatenews.org/news/18062019/climate-change-tipping-points-microbes-health-soil-oceans-viruses-bacteria

http://www.bbc.com/earth/story/20170504-there-are-diseases-hidden-in-ice-and-they-are-waking-up

{% spotify http://open.spotify.com/artist/6FBDaR13swtiWwGhX1WQsP %}

