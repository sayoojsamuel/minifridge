---
title: Mystery of Lake Hillier
date: 2019-09-22 03:47:07
thumbnail: gallery/header-lake.jpg
categories:
- Info
- Facts
tags:
- Mystery
- Pink Lake
- Hiller
---

# The Pink Lake in Australia

This blog is for enlightening about the mystery of the pink lake Lake Hillier in Australia. Lake Hillier is a saline lake situated off the south coast of Western Australia. It has now become a famous tourist attraction due to its bright pink color. But the main cause of the coloration was always a mystery.

An investigation was conducted in finding out the reason for the pink color of the lake. The investigating team collected samples from the lake and then conducted a metagenomic analysis is which DNA is extracted to identify the species. It was revealed that among other microorganisms collected in Lake Hillier’s samples, an algae named Dunaliella salina is thought to be the main culprit for the pink coloration of the lake. The D.salina algae produces pigment compounds called carotenoids. These absorb sunlight and also gives the algae a reddish-pink coloring.

{% asset_img lakemiller.jpg "Lake Hiller" %}

But D.salina is not the only culprit for the coloration. Scientists also found other red-colored microbes like bacterias like Salinibacter ruber, Dechloromonas aromatic and some archaea in the samples. The bacteria Dechloromonas aromatic breaks down compounds like benzene and toluene.  So with this information researchers found that Lake Hillier was once used as a leather tanning station in the 1900s.

{% asset_img lakerebta.jpg "Lake Rebta" %}

Lake Hillier is not the only pink lake present in the world. Pink lakes are present in many countries around the world like Senegal, Canada, Spain, and Azerbaijan. These are natural phenomena that attract tourists from around the world. Everywhere the main cause of the pink color is the algae Dunaliella salina.

;)
